-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 10, 2017 at 03:43 PM
-- Server version: 5.7.19
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pincode`
--

-- --------------------------------------------------------

--
-- Table structure for table `pincodeentry_details`
--

CREATE TABLE `pincodeentry_details` (
  `id` int(50) NOT NULL,
  `user_id` int(50) NOT NULL,
  `pincode` bigint(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `blue_dart` varchar(50) NOT NULL,
  `xpress` char(50) NOT NULL,
  `delivery` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pincodeentry_details`
--

INSERT INTO `pincodeentry_details` (`id`, `user_id`, `pincode`, `city`, `state`, `blue_dart`, `xpress`, `delivery`) VALUES
(1, 3, 624003, 'Dindigul', 'Tamilnadu', 'No', 'Yes', 'No'),
(2, 1, 623707, 'Paramakudi', 'Tamilnadu', 'No', 'Yes', 'No'),
(3, 2, 623708, 'Ramanathapuram', 'Tamilnadu', 'Yes', 'No', 'No'),
(4, 1, 600001, 'Urappakkam', 'Bihar', 'Yes', 'No', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `registration_details`
--

CREATE TABLE `registration_details` (
  `user_id` int(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_mobile` bigint(100) NOT NULL,
  `user_password` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_details`
--

INSERT INTO `registration_details` (`user_id`, `user_name`, `user_email`, `user_mobile`, `user_password`) VALUES
(1, 'Ramanathan', 'ramanathan@gmail.com', 8124843393, '202cb962ac59075b964b07152d234b70'),
(2, 'Geetha', 'geetha@gmail.com', 8124843365, 'caf1a3dfb505ffed0d024130f58c5cfa'),
(3, 'Harikumar', 'hari@gmail.com', 8998899889, 'a9bcf1e4d7b95a22e2975c812d938889');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE `subscriber` (
  `id` int(10) NOT NULL,
  `s_email` varchar(100) NOT NULL,
  `s_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriber`
--

INSERT INTO `subscriber` (`id`, `s_email`, `s_date`) VALUES
(1, 'oxsoftwares@gmail.com', '2017-08-09 20:59:42'),
(2, 'ramanathan@oxsoftwares.com', '2017-08-09 21:01:40'),
(3, 'jaya@oxsoftwares.com', '2017-08-09 21:02:01'),
(4, 'geetha@gmail.com', '2017-08-09 21:02:22'),
(5, 'hari@gmail.com', '2017-08-09 21:04:47'),
(6, 'karthic@oxsoftwares.com', '2017-08-09 21:42:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pincodeentry_details`
--
ALTER TABLE `pincodeentry_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `registration_details`
--
ALTER TABLE `registration_details`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pincodeentry_details`
--
ALTER TABLE `pincodeentry_details`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `registration_details`
--
ALTER TABLE `registration_details`
  MODIFY `user_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pincodeentry_details`
--
ALTER TABLE `pincodeentry_details`
  ADD CONSTRAINT `pincodeentry_details_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `registration_details` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
