<?php
session_start();
if (isset($_SESSION['sessionuser'])) {
    header('location:adminarea.php');
}
?>
<html>
    <head>
        <title>Login Page</title>
        <link rel="stylesheet" type="text/css" href="pincodesearch.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="pincodesearch.js"></script>
    </head>
    <body>
        <header id="top">
            <div id="logo">
                <a href="index.php" title="Pincode">
                    <img height="auto" width="auto" src="upload/pinlogo.png" alt="" title="" />
                </a>
            </div>
            <div id="navigation">
                <ul id="menu-item">
                    <li id="menu-item-1"><a href="index.php"><span title="Home">HOME</span></a></li>
                    <li id="menu-item-2"><a href="register.php"><span title="Register">REGISTER</span></a></li>
                </ul>
            </div>
        </header>
        <div id="login">
            <div id="login-title"><h1>Login</h1></div>
            <form id="form" method="post" action="function_calling.php"> 
                <div id="login-page">
                    <div id="text-field">
                        <div class="text"><label>Email </label></div>
                        <div id="field"><input type="email" id="emailID" name="Mailid" placeholder=" Email ID"/></div>
                        <div id="error"> <span id="emailIDerror"></span></div>
                    </div>
                    <div id="text-field">
                        <div class="text"><label>Password </label></div>
                        <div id="field"><input type="password" id="password" name="Password" placeholder=" Password"/></div>
                        <div id="error"><span id="passworderror"></span>
                            <?php
                            if (isset($_SESSION['message'])) {
                                echo $_SESSION['message'];
                                unset($_SESSION['message']);
                            }
                            ?>
                        </div>
                    </div> 
                    <div id="text-field">
                        <input type="submit" name="login-submit" id="login-submit" value="LOGIN" />
                        <div class="message-display submit"></div> 

                        <p class="notmember">you are not a member <a href="register.php" id="register-here">Register here!</a> </p>
                    </div>
                </div>
            </form>
        </div>

        <footer id="bottom">
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <ul id="socialmedia">
                    <li><a href="https://www.facebook.com/oxsoftwares" target="_blank" title="Find us on Facebook">Facebook</a></li>
                    <li><a href="https://twitter.com/oxsoftwares" target="_blank" title="Find us on Twitter">Twitter</a></li>
                    <li><a href="https://plus.google.com/+oxsoftwares" target="_blank" title="Find us on Google+">Google+</a></li>
                    <li><a href="https://www.linkedin.com/company/oxsoftwares" target="_blank" title="Find us on LinkedIn">LinkedIn</a></li>
                </ul>
            </div>
            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
            <div id="subscribe">
                <form method="post" id="newsletter-signup" action="subscribe.php?action=signup">
                    <input type="email" name="signup-email" id="newsletter-email" placeholder="Enter email to subscribe" required/>
                    <input type="submit" id="newsletter-submit" title="Subscribe" value="SUBSCRIBE" />
                    <div id="err"><span id="signup-response"></span></div>
                </form>
            </div>
        </div>
        <div id="copyright">
            2017 &copy; OX SoftwareS. All rights reserved.
        </div>
    </footer>
</body>
</html>
