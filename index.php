<?php
session_start();
if (isset($_SESSION['sessionuser'])) {
    header('location:adminarea.php');
}
?>
<html>
    <head>
        <title>PIN Search</title>
        <link rel="stylesheet" type="text/css" href="pincodesearch.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="pincodesearch.js"></script>
    </head>
    <body>
        <header id="top">
            <div id="logo">
                <a href="index.php" title="OX Softwares">
                    <img height="auto" width="auto" src="upload/pinlogo.png" alt="" title="" />
                </a>
            </div>
            <div id="navigation">
                <ul id="menu-item">
                    <li id="menu-item-1"><a href="register.php"><span title="Register">REGISTER</span></a></li>
                    <li id="menu-item-2"><a href="login.php"><span title="Login">LOGIN</span></a></li>
                </ul>
            </div>
        </header>
        <div id="home">
            <div id="search-title"><h1>Search</h1></div>
            <form id="form" method="post" action="function_calling.php"> 
                <div id="pincode-search">
                    <label id="pincode-text"> Enter Your Pincode Here </label>
                    <div id="search-button">
                        <input type="text" id="pincodeSearch" name="pincodeSearch"  placeholder=" Pincode" required/>
                        <div id="errorpincode"><span id="pincodeSearcherror"></span></div>
                    </div> 
                    <div>
                        <input type="submit" id="search" name="search" value="Search"/>
                    </div>


                    <div id='cod'>
                        <?php
                        if (isset($_SESSION['pincode']))
                            echo "COD is available at " . $_SESSION['pincode'];
                        unset($_SESSION['pincode']);
                        ?>
                    </div>
                    <div id="cod">
                        <?php
                        if (isset($_SESSION['blue_dart'])) {
                            echo "Blue_dart:  " . $_SESSION['blue_dart'];
                            unset($_SESSION['blue_dart']);
                        }
                        ?>
                    </div>
                    <div id="cod">
                        <?php
                        if (isset($_SESSION['xpress'])) {
                            echo "Xpress:  " . $_SESSION['xpress'];
                            unset($_SESSION['xpress']);
                        }
                        ?>
                    </div>
                    <div id="cod">
                        <?php
                        if (isset($_SESSION['delivery'])) {
                            echo "Delivery  " . $_SESSION['delivery'];
                            unset($_SESSION['delivery']);
                        }
                        ?>
                    </div>
                    <div  id='coderror'>
                        <?php
                        if (isset($_SESSION['message'])) {
                            echo $_SESSION['message'];
                            unset($_SESSION['message']);
                        }
                        ?>
                    </div>
                    <div  id='coderror'>
                        <?php
                        if (isset($_SESSION['msg'])) {
                            echo $_SESSION['msg'];
                            unset($_SESSION['msg']);
                        }
                        ?>
                    </div>

                    </table>
                </div>

            </form>
        </div>
        <footer id="bottom">
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <ul id="socialmedia">
                    <li><a href="https://www.facebook.com/oxsoftwares" target="_blank" title="Find us on Facebook">Facebook</a></li>
                    <li><a href="https://twitter.com/oxsoftwares" target="_blank" title="Find us on Twitter">Twitter</a></li>
                    <li><a href="https://plus.google.com/+oxsoftwares" target="_blank" title="Find us on Google+">Google+</a></li>
                    <li><a href="https://www.linkedin.com/company/oxsoftwares" target="_blank" title="Find us on LinkedIn">LinkedIn</a></li>
                </ul>
            </div>
            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
            <div id="subscribe">
                <form method="post" id="newsletter-signup" action="subscribe.php?action=signup">
                    <input type="email" name="signup-email" id="newsletter-email" placeholder="Enter email to subscribe" required/>
                    <input type="submit" id="newsletter-submit" title="Subscribe" value="SUBSCRIBE" />
                    <div id="err"><span id="signup-response"></span></div>
                </form>
            </div>
        </div>
        <div id="copyright">
            2017 &copy; OX SoftwareS. All rights reserved.
        </div>
    </footer>
</body>
</html>
