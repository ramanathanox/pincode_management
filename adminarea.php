<?php
session_start();
$user = $_SESSION['sessionuser'];
$id = $_SESSION['sessionid'];

if (!isset($user)) {
    header('location:index.php');
}
include ('database.php');
$dbconnection = new Database();
//$entrymessage = '';
//if (isset($_POST['pincode-submit'])) {
//    $id = $_SESSION['sessionid'];
//    $pincode = $_POST['Pincodename'];
//    $cityname = $_POST['Cityname'];
//    $statename = $_POST['Statename'];
//    $bluedartcouriers = $_POST['Bluedartcouriers'];
//    $xpresscouriers = $_POST['Xpresscouriers'];
//    $deliverycouriers = $_POST['Deliverycouriers'];
//    $dbconnection->pincodeentry_details($id, $pincode, $cityname, $statename, $bluedartcouriers, $xpresscouriers, $deliverycouriers);
//    $entrymessage = "<p>Entry details are registered <a href='adminarea.php' color='whitesmoke'> click OK</a></p>";
//}
?>

<html>
    <head>
        <title><?php echo $user; ?></title>
        <link rel="stylesheet" type="text/css" href="pincodesearch.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="pincodesearch.js"></script>
    </head>
    <body>
        <header id="top">
            <div id="logo">
                <a href="index.php" title="OX Softwares">
                    <img height="auto" width="auto" src="upload/pinlogo.png" alt="" title="" />
                </a>
            </div>
            <div id="navigation">
                <ul id="menu-item">
                    <li id="menu-item-1"><a href="logout.php"><span title="Logout">Logout</span></a></li>                  
                </ul>
            </div>
            <div id="welcome_user"> <?php
                echo "<h2>Welcome $user</h2>";
                ?>
            </div>
        </header>
        <div id="adminarea">
            <div id="admin-title"><h1>Pin-code</h1></div>
            <form id="form" method="post" action="function_calling.php" enctype="multipart/form-data"> 

                <div id="admin-page">
                    <div id="text-field">
                        <div class="text"><label > Pin-code </label></div>
                        <div id="field">
                            <input type="text" id="pincodeName" name="Pincodename"  placeholder=" Pin-code" 
                                   value="<?php
                                   if (isset($_SESSION['pincode'])) {
                                       echo $_SESSION['pincode'];
                                   } else {
//                                       $_POST['Pincodename'] = "";
                                       unset($_SESSION['pincode']);
                                   }
                                   ?>"required/>
                        </div>
                        <div id="error"><span id="pincodeNameerror"></span> </div>
                    </div> 
                    <div id="text-field">
                        <div class="text"><label > City</label></div>
                        <div id="field">
                            <input type="text" id="cityName" name="Cityname"  placeholder=" City" 
                                   value="<?php
                                   if (isset($_SESSION['city'])) {
                                       echo $_SESSION['city'];
                                   } else {
                                       unset($_SESSION['city']);
//                                       unset($_SESSION['state']);
//                                       unset($_SESSION['blue_dart']);
//                                       unset($_SESSION['xpress']);
//                                       unset($_SESSION['delivery']);
                                   }
                                   ?>"required/>
                        </div>
                        <div id="error"><span id="cityNameerror"></span> </div>
                    </div> 
                    <div id="text-field">
                        <div class="text"><label > State</label></div>
                        <div id="field"> 
                            <select id="stateName" name="Statename" required>
                                <option><?php
                                    if (isset($_SESSION['state'])) {
                                        echo $_SESSION['state'];
                                    } else {
//                                       $_POST['Pincodename'] = "";
                                        unset($_SESSION['state']);
                                        echo "-----------select-----------";
                                    }
                                    ?>
                                </option>
                                <option value="Tamilnadu" name="state">Tamilnadu</option>
                                <option value="Karnataka" name="state">Karnataka</option>
                                <option value="Andhra Pradesh" name="state">Andhra Pradesh</option>
                                <option value="Arunachal Pradesh" name="state">Arunachal Pradesh</option>
                                <option value="Assam" name="state">Assam</option>
                                <option value="Bihar" name="state">Bihar</option>
                                <!--New Delhi||Andaman/Nicobar Islands|Andhra Pradesh|Arunachal Pradesh|Assam|Bihar|Chandigarh|Chhattisgarh|Dadra/Nagar Haveli|Daman/Diu|Goa|Gujarat|Haryana|Himachal Pradesh|Jammu/Kashmir|Jharkhand|Karnataka|Kerala|Lakshadweep|Madhya Pradesh|Maharashtra|Manipur|Meghalaya|Mizoram|Nagaland|Orissa|Pondicherry|Punjab|Rajasthan|Sikkim|Tamil Nadu|Tripura|Uttaranchal|Uttar Pradesh|West Bengal';-->
                            </select>
                        </div>
                        <div id="error"><span id="stateNameerror"></span> </div>
                    </div>
                    <div id="text-field">
                        <div class="text"><label>COURIERS :</label> </div>
                    </div>
                    <div id="text-field">
                        <div class="text"><label>Bluedart </label></div>
                        <div id="field"> 
                            <input type="radio" id="bluedartCouriers" name="Bluedartcouriers"
                                   value="<?php
//                                   $_POST['blue_dart'] = $_SESSION['blue_dart'];
                                   if (isset($_SESSION['blue_dart']) == "Yes") {
                                       echo "Yes";                                   } else {
                                       unset($_SESSION['blue_dart']);
                                       echo "Yes";
                                   }
                                   ?>" required/><span>Yes</span>
                            <input type="radio" id="bluedartCouriers" name="Bluedartcouriers"
                                   value="<?php
//                                 $_POST['blue_dart'] = $_SESSION['blue_dart'];
                                   if (isset($_SESSION['blue_dart']) == "No") {
                                      echo "No";
                                   } else {
                                       unset($_SESSION['blue_dart']);
                                       echo "No";
                                   }
                                   ?>" required/><span>No</span>
                        </div>
                        <div id="error"><span id="bluedartCourierserror"></span></div>
                    </div>
                    <div id="text-field">
                        <div class="text"><label>Xpress </label></div>
                        <div id="field"> 
                            <select id="xpressCouriers" name="Xpresscouriers" required>
                                <option><?php
                                    if (isset($_SESSION['xpress'])) {
                                        echo $_SESSION['xpress'];
                                    } else {
//                                       $_POST['Pincodename'] = "";
                                        unset($_SESSION['xpress']);
                                        echo "-----------select-----------";
                                    }
                                    ?>
                                </option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div id="error"><span id="xpressCourierserror"></span></div>
                    </div> 
                    <div id="text-field">
                        <div class="text"><label>Delivery </label></div>
                        <div id="field"> 
                            <select id="deliveryCouriers" name="Deliverycouriers" required>
                                <option><?php
                                    if (isset($_SESSION['delivery'])) {
                                        echo $_SESSION['delivery'];
                                    } else {
//                                       $_POST['Pincodename'] = "";
                                        unset($_SESSION['delivery']);
                                        echo "-----------select-----------";
                                    }
                                    ?>
                                </option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div id="error"><span id="deliveryCourierserror"></span></div>
                    </div> 
                    <div id="text-field">
                        <?php if (isset($_SESSION['id'])) { ?>
                            <input type='hidden' name="pin_id" value="<?php echo $_SESSION['id']; ?>"/>
                            <input type="submit" name="pincode-update" id="pincode-update" value="UPDATE" />
                            <input type="submit" name="pincode-cancel" id="pincode-cancel" value="CANCEL" />
                            <!--                            <a href="function_calling.php" name="cancel">cancel</a>-->
                        <?php } else { ?>
                            <input type="submit" name="pincode-submit" id="pincode-submit" value="SUBMIT" />
                        <?php } ?>
                        <div class="message-display submit"></div> 
                    </div>
                </div>    
            </form>
            <?php
            if (isset($entrymessage)) {
                echo $entrymessage;
            }
            ?>
            <!--<div id="text-field">
               <input type="submit" name="update" id="pincode-update" value="UPDATE" />
               <div class="message-display update"></div> 
           </div>-->
            <table width = '100%' align = 'center' border-colr:445464 color="white" ><th id='table_content'>Pincode</th><th id='table_content'>City</th><th id='table_content'>State</th><th id='table_content'>Blue_dart</th><th id='table_content'>Xpress</th><th id='table_content'>Delivery</th><th id='table_content'>Edit</th><th id='table_content'>Delete</th>
                <?php
                $pincode_details_login = $dbconnection->login_display($id);
                foreach ($pincode_details_login as $pincode_login_keys) {
                    ?>         
                    <tr align='center'>

                    <input type='hidden' name="pin_id" value="<?php echo $pincode_login_keys['id']; ?>"/>
                    <td name="pincode" value="<?php echo $pincode_login_keys['pincode']; ?>"><?php echo $pincode_login_keys['pincode']; ?></td>
                    <td name="city" value="<?php echo $pincode_login_keys['city']; ?>"> <?php echo $pincode_login_keys['city']; ?></td>
                    <td name="state" value="<?php echo $pincode_login_keys['state']; ?>"> <?php echo $pincode_login_keys['state']; ?></td>
                    <td name="blue_dart" value="<?php echo $pincode_login_keys['blue_dart']; ?>"> <?php echo $pincode_login_keys['blue_dart']; ?></td>
                    <td name="xpress" value="<?php echo $pincode_login_keys['xpress']; ?>"> <?php echo $pincode_login_keys['xpress']; ?></td>
                    <td name="delivery" value="<?php echo $pincode_login_keys['delivery']; ?>"> <?php echo $pincode_login_keys['delivery']; ?></td>
                    <td><a href="edit_delete.php?edit_id=<?php echo $pincode_login_keys['id']; ?>" name="edit">Edit</a></td>
                    <td><a href="edit_delete.php?id=<?php echo $pincode_login_keys['id']; ?>" name="delete">Delete</a></td>

                    </tr>
                <?php } ?>
            </table>
        </div>
        <footer id="bottom">
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <ul id="socialmedia">
                    <li><a href="https://www.facebook.com/oxsoftwares" target="_blank" title="Find us on Facebook">Facebook</a></li>
                    <li><a href="https://twitter.com/oxsoftwares" target="_blank" title="Find us on Twitter">Twitter</a></li>
                    <li><a href="https://plus.google.com/+oxsoftwares" target="_blank" title="Find us on Google+">Google+</a></li>
                    <li><a href="https://www.linkedin.com/company/oxsoftwares" target="_blank" title="Find us on LinkedIn">LinkedIn</a></li>
                </ul>
            </div>
            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
            <div id="subscribe">
                <form method="post" id="newsletter-signup" action="subscribe.php?action=signup">
                    <input type="email" name="signup-email" id="newsletter-email" placeholder="Enter email to subscribe" required/>
                    <input type="submit" id="newsletter-submit" title="Subscribe" value="SUBSCRIBE" />
                    <div id="err"><span id="signup-response"></span></div>
                </form>
            </div>
        </div>
        <div id="copyright">
            2017 &copy; OX SoftwareS. All rights reserved.
        </div>
    </footer>
</body>
</html>
