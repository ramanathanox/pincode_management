<?php

class Database {

    public $dbcon_host = 'localhost';
    public $dbcon_pass = 'admin@123';
    public $dbcon_user = 'root';
    public $dbcon_name = 'pincode';
    public $dbcon;

    public function db_connect() {
        $this->dbcon = mysqli_connect($this->dbcon_host, $this->dbcon_user, $this->dbcon_pass, $this->dbcon_name);
        if (mysqli_connect_errno()) {
            echo "connection failes" . mysqli_connect_error();
            exit();
        }
        return TRUE;
    }

    public function registration_details() {
        $this->db_connect();
        if (isset($_POST['register-submit'])) {
            $userName = $_POST['Firstname'];
            $userMailid = $_POST['Mailid'];
            $userMobile = $_POST['Mobile'];
            $userPassword = md5($_POST['Password']);
            $sqlinsert = "insert into registration_details(user_name,user_email,user_mobile,user_password) values('$userName','$userMailid','$userMobile','$userPassword')";
            $result = mysqli_query($this->dbcon, $sqlinsert);
            if ($result) {
                header('location:login.php');
            } else {
                echo 'not insert';
            }
        }
    }

    public function login_details($userMailid, $userPassword) {
        $this->db_connect();
        $sqllogin = "select * from registration_details where user_email='$userMailid' and user_password='$userPassword'";
        $result = mysqli_query($this->dbcon, $sqllogin);

        if (mysqli_num_rows($result) == 1) {
            while ($row = mysqli_fetch_assoc($result)) {
                $dbusername = $row['user_name'];
                $dbuserid = $row['user_id'];
                session_start();
                $_SESSION['sessionuser'] = $dbusername;
                $_SESSION['sessionid'] = $dbuserid;
                header('location:adminarea.php');
            }
        } else {
//                $_SESSION['message'] = 1;
            $_SESSION['message'] = "Password is invalid";
//            die(header('refresh: 3; url=login.php') . 'Invalid Credentials, wait 3 seconds or just click <a href="index.php">HERE</a> to check again.');
        }
    }

    public function login_display($id) {
        $this->db_connect();
        $pincode_details_login = array();
        $sql_pincode_select = "select id,pincode,city,state,blue_dart,xpress,delivery from pincodeentry_details where pincodeentry_details.user_id='$id' ";
        $result_select = mysqli_query($this->dbcon, $sql_pincode_select);
        while ($row = mysqli_fetch_assoc($result_select)) {
            $pincode_details_login[] = $row;
        }
        return $pincode_details_login;
    }

    public function pincodeentry_details($id, $pincode, $cityname, $statename, $bluedartcouriers, $xpresscouriers, $deliverycouriers) {
        $this->db_connect();
        $sql_pincode_insert = "insert into pincodeentry_details(user_id,pincode,city,state,blue_dart,xpress,delivery) values('$id','$pincode','$cityname','$statename','$bluedartcouriers','$xpresscouriers','$deliverycouriers')";
        $result = mysqli_query($this->dbcon, $sql_pincode_insert);
        if ($result) {
            return TRUE;
//            while ($row = mysqli_fetch_assoc($result)) {
//                $_SESSION['dbpincode'] = $row['pincode'];
//                $_SESSION['dbcity'] = $row['city'];
//                $_SESSION['dbstate'] = $row['state'];
//                $_SESSION['dbblue_dart'] = $row['blue_dart'];
//                $_SESSION['dbxpress'] = $row['xpress'];
//                $_SESSION['dbdelivery'] = $row['delivery'];
//        }
        } else {
            echo 'error';
        }
    }

    public function pincodeupdate_details($row_id, $pincode, $cityname, $statename, $bluedartcouriers, $xpresscouriers, $deliverycouriers) {
        $this->db_connect();
        $sql_pincode_update = "update pincodeentry_details set pincode='$pincode', city='$cityname', state='$statename', blue_dart='$bluedartcouriers', xpress='$xpresscouriers', delivery='$deliverycouriers' where id='$row_id'";
        $result = mysqli_query($this->dbcon, $sql_pincode_update);
        if ($result) {

            $_SESSION['message'] = "Please Confirm Your Edit";
            return TRUE;
            echo 'correct';
        } else {
            echo 'error';
        }
    }

    public function search_pincode($searching_pincode) {
        $this->db_connect();
//        $search_details = array();
        $yes = 'Yes';
        $sql_search_pincode = "select pincode,blue_dart,xpress,delivery from pincodeentry_details where pincode='$searching_pincode'";
        $result_search = mysqli_query($this->dbcon, $sql_search_pincode);
        if (mysqli_num_rows($result_search) > NULL) {
            while ($row = mysqli_fetch_assoc($result_search)) {
//                if ($row > NULL) {
//            $search_details[] = $row;
                $pincode = $row['pincode'];
                $blue_dart = $row['blue_dart'];
                $xpress = $row['xpress'];
                $delivery = $row['delivery'];

                if ($blue_dart == $yes || $xpress == $yes || $delivery == $yes) {
                    $_SESSION['pincode'] = $pincode;
                    $_SESSION['blue_dart'] = $blue_dart;
                    $_SESSION['xpress'] = $xpress;
                    $_SESSION['delivery'] = $delivery;
                    header('location:index.php');
                } else {
                    $_SESSION['message'] = "COD is not available for this Pin-code";
                    header('location:index.php');
                }
//                }
            }
        } else {
            $_SESSION['msg'] = "Your Pin-code is not available";
            header('location:index.php');
        }
    }

    public function pincode_delete($pincode_id) {
        $this->db_connect();
        $sql_delete = "delete from pincodeentry_details where id='$pincode_id'";
        $result_delete = mysqli_query($this->dbcon, $sql_delete);
        if ($result_delete) {
            return TRUE;
        }
    }

    public function pincode_edit($pincode_id) {
        $this->db_connect();
//        $edit_array = array();
        $sql_edit = "select * from pincodeentry_details where id='$pincode_id'";
        $result_edit = mysqli_query($this->dbcon, $sql_edit);
        while ($row = mysqli_fetch_assoc($result_edit)) {
//            $edit_array[] = $row;
            $_SESSION['id'] = $row['id'];

            $_SESSION['pincode'] = $row['pincode'];
            $_SESSION['city'] = $row['city'];
            $_SESSION['state'] = $row['state'];
            $_SESSION['blue_dart'] = $row['blue_dart'];
            $_SESSION['xpress'] = $row['xpress'];
            $_SESSION['delivery'] = $row['delivery'];
            header('location:adminarea.php');
        }
//        return $edit_array;
    }

    public function newsletter($email_newsletter) {
        $this->db_connect();
        $sql_email = "insert into subscribed_email(email) values('$email_newsletter')";
        $result_newsletter = mysqli_query($this->dbcon, $sql_email);
        if (!$result_newsletter) {
            echo 'error';
        }
    }

}
?>

