<?php

session_start();
include ('database.php');
$dbconnection = new Database();

if (isset($_POST['search'])) {
    $searching_pincode = $_POST['pincodeSearch'];
//    $array[]=array();
//    $array = $dbconnection->search_pincode($searching_pincode);
//    header("location:index.php?array=".urlencode(serialize($array)));
    $dbconnection->search_pincode($searching_pincode);
//    header('location:index.php');
//    $_SESSION['array'] = $array;
//    header("location:index.php?array=" . serialize($array));
}
if (isset($_POST['login-submit'])) {
    $userMailid = $_POST['Mailid'];
    $userPassword = md5($_POST['Password']);
    $dbconnection->login_details($userMailid, $userPassword);
    header('location:login.php');
}
if (isset($_POST['pincode-submit'])) {
    $id = $_SESSION['sessionid'];
    $pincode = $_POST['Pincodename'];
    $cityname = $_POST['Cityname'];
    $statename = $_POST['Statename'];
    $bluedartcouriers = $_POST['Bluedartcouriers'];
    $xpresscouriers = $_POST['Xpresscouriers'];
    $deliverycouriers = $_POST['Deliverycouriers'];
    $dbconnection->pincodeentry_details($id, $pincode, $cityname, $statename, $bluedartcouriers, $xpresscouriers, $deliverycouriers);
    $entrymessage = "<p>Entry details are registered <a href='adminarea.php' color='whitesmoke'> click OK</a></p>";
    header('location:adminarea.php');
}
if (isset($_POST['pincode-cancel'])) {
    unset($_SESSION['id']);
    unset($_SESSION['pincode']);
    unset($_SESSION['city']);
    unset($_SESSION['state']);
    unset($_SESSION['blue_dart']);
    unset($_SESSION['xpress']);
    unset($_SESSION['delivery']);
    header('location:adminarea.php');
}
if (isset($_POST['pincode-update'])) {
    $id = $_POST['pin_id'];
    $pincode = $_POST['Pincodename'];
    $cityname = $_POST['Cityname'];
    $statename = $_POST['Statename'];
    $bluedartcouriers = $_POST['Bluedartcouriers'];
    $xpresscouriers = $_POST['Xpresscouriers'];
    $deliverycouriers = $_POST['Deliverycouriers'];
    $dbconnection->pincodeupdate_details($id, $pincode, $cityname, $statename, $bluedartcouriers, $xpresscouriers, $deliverycouriers);
    unset($_SESSION['id']);
    unset($_SESSION['pincode']);
    unset($_SESSION['city']);
    unset($_SESSION['state']);
    unset($_SESSION['blue_dart']);
    unset($_SESSION['xpress']);
    unset($_SESSION['delivery']);
    header('location:adminarea.php');
}
?>