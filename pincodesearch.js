$(document).ready(function () {
    $("#register-submit").click(function () {
        var firstName = $('#firstName').val();

        if (firstName == "") {
            $("#firstNameerror").css("display", "inline");
            $('#firstNameerror').html("This field is required");
            $("input").focus(function () {
                $("#firstNameerror").css("display", "none");
            });
            return false;
        }
        var firstnameREGEX = /^[A-Za-z]{3,16}$/;
        if (!firstnameREGEX.test(firstName)) {
            $("#firstNameerror").css("display", "inline");
            $('#firstNameerror').html("Name is incorrect");
            $("input").focus(function () {
                $("#firstNameerror").css("display", "none");
            });
            return false;
        }
        var emailID = $('#emailID').val();

        if (emailID == "") {
            $("#emailIDerror").css("display", "inline");
            $("#emailIDerror").html("This field is required");
            $("input").focus(function () {
                $("#emailIDerror").css("display", "none");
            });
            return false;
        }
        var emailREGEX = /^[a-z,A-Z]+([\._ ]?(\w{1,9})+)*@[a-z]+([\._]?\w+)*(\.[a-z]{2,3})$/;
        if (!emailREGEX.test(emailID)) {
            $("#emailIDerror").css("display", "inline");
            $("#emailIDerror").html("Email is incorrect");
            $("input").focus(function () {
                $("#emailIDerror").css("display", "none");
            });
            return false;
        }
        var mobileNumber = $('#mobileNumber').val();
        if (mobileNumber == "")
        {
            $("#mobileNumbererror").css("display", "inline");
            $("#mobileNumbererror").html("This field is required");
            $("input").focus(function () {
                $("#mobileNumbererror").css("display", "none");
            });
            return false;
        }
        mobilenumberREGEX = /^[0-9]{10}$/;
        if (!mobilenumberREGEX.test(mobileNumber)) {
            $("#mobileNumbererror").css("display", "inline");
            $('#mobileNumbererror').html("Mobile no must 10 digit");
            $("input").focus(function () {
                $("#mobileNumbererror").css("display", "none");
            });
            return false;
        }

        var password = $('#password').val();
        if (password == "") {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html("This field is required");
            $("input").focus(function () {
                $("#passworderror").css("display", "none");
            });
            return false;
        }
        var confirmpassword = $('#confirmpassword').val();
        if (confirmpassword == "") {
            $("#confirmpassworderror").css("display", "inline");
            $("#confirmpassworderror").html("This field is required");
            $("input").focus(function () {
                $("#confirmpassworderror").css("display", "none");
            });
            return false;
        }
        var password = $('#password').val();
        var confirmpassword = $('#confirmpassword').val();
        if (password != confirmpassword) {
            $("#matchedpassworderror").css("display", "inline");
            $("#matchedpassworderror").html("Password does't matched");
            $("input").focus(function () {
                $("#matchedpassworderror").css("display", "none");
            });
            return false;
        }

    });
    $("#login-submit").click(function () {
        var emailID = $('#emailID').val();
        if (emailID == "") {
            $("#emailIDerror").css("display", "inline");
            $("#emailIDerror").html("This field is required");
            $("input").focus(function () {
                $("#emailIDerror").css("display", "none");
            });
            return false;
        }
        var emailREGEX = /^\w+([\._ ]?\w+)*@\w+([\._]?\w+)*(\.\w{2,3})$/;
        if (!emailREGEX.test(emailID)) {
            $("#emailIDerror").css("display", "inline");
            $("#emailIDerror").html("Email is incorrect");
            $("input").focus(function () {
                $("#emailIDerror").css("display", "none");
            });
            return false;
        }
        var password = $('#password').val();
        if (password == "") {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html("This field is required");
            $("input").focus(function () {
                $("#passworderror").css("display", "none");
            });
            return false;
        }
    });

    $("#pincode-submit").click(function () {
        var pincodeName = $('#pincodeName').val();
        if (pincodeName == "")
        {
            $("#pincodeNameerror").css("display", "inline");
            $("#pincodeNameerror").html("This field is required");
            $("input").focus(function () {
                $("#pincodeNameerror").css("display", "none");
            });
            return false;
        }
        pincodeNameREGEX = /^[0-9]{6}$/;
        if (!pincodeNameREGEX.test(pincodeName)) {
            $("#pincodeNameerror").css("display", "inline");
            $('#pincodeNameerror').html("Pincode must 6 digit");
            $("input").focus(function () {
                $("#pincodeNameerror").css("display", "none");
            });
            return false;
        }
        var cityName = $('#cityName').val();
        if (cityName == "") {
            $("#cityNameerror").css("display", "inline");
            $("#cityNameerror").html("This field is required");
            $("input").focus(function () {
                $("#cityNameerror").css("display", "none");
            });
            return false;
        }
        var stateName = $('#stateName').val();
        if (stateName == "") {
            $("#stateNameerror").css("display", "inline");
            $("#stateNameerror").html("This field is required");
            $("select").focus(function () {
                $("#stateNameerror").css("display", "none");
            });
            return false;
        }
        var bluedartCouriers = $('#bluedartCouriers').val();
        if ($("#bluedartCouriers:checked").length == 0) {
            $("#bluedartCourierserror").css("display", "inline");
            $("#bluedartCourierserror").html("This field is required");
            $("input").focus(function () {
                $("#bluedartCourierserror").css("display", "none");
            });
            return false;
        }
        var xpressCouriers = $('#xpressCouriers').val();
        if (xpressCouriers == "") {
            $("#xpressCourierserror").css("display", "inline");
            $("#xpressCourierserror").html("This field is required");
            $("select").focus(function () {
                $("#xpressCourierserror").css("display", "none");
            });
            return false;
        }
        var deliveryCouriers = $('#deliveryCouriers').val();
        if (deliveryCouriers == "") {
            $("#deliveryCourierserror").css("display", "inline");
            $("#deliveryCourierserror").html("This field is required");
            $("select").focus(function () {
                $("#deliveryCourierserror").css("display", "none");
            });
            return false;
        }
    });
    $("#search").click(function () {
        var pincodeSearch = $('#pincodeSearch').val();
        if (pincodeSearch == "") {
            $("#pincodeSearcherror").css("display", "inline");
            $('#pincodeSearcherror').html("This field is required");
            $("input").focus(function () {
                $("#pincodeSearcherror").css("display", "none");
            });
            return false;
        }
        pincodeNameREGEX = /^[0-9]{6}$/;
        if (!pincodeNameREGEX.test(pincodeSearch)) {
            $("#pincodeSearcherror").css("display", "inline");
            $('#pincodeSearcherror').html("Pincode must 6 digit");
            $("input").focus(function () {
                $("#pincodeSearcherror").css("display", "none");
            });
            return false;
        }
    });

    
    $('#newsletter-signup').submit(function () {

        //setup variables
        var form = $(this),
                formData = form.serialize(),
                formUrl = form.attr('action'),
                formMethod = form.attr('method'),
                responseMsg = $('#signup-response');

        //show response message - waiting
        responseMsg.hide()
                .addClass('response-waiting')
                .text('Please Wait...')
                .fadeIn(200);

        //send data to server for validation
        $.ajax({
            url: formUrl,
            type: formMethod,
            data: formData,
            success: function (data) {

                //setup variables
                var responseData = jQuery.parseJSON(data),
                        klass = '';

                //response conditional
                switch (responseData.status) {
                    case 'error':
                        klass = 'response-error';
                        break;
                    case 'success':
                        klass = 'response-success';
                        break;
                }

                //show reponse message
                responseMsg.fadeOut(200, function () {
                    $(this).removeClass('response-waiting')
                            .addClass(klass)
                            .text(responseData.message)
                            .fadeIn(200, function () {
                                //set timeout to hide response message
                                setTimeout(function () {
                                    responseMsg.fadeOut(200, function () {
                                        $(this).removeClass(klass);
                                    });
                                }, 3000);
                            });
                });
            }
        });

        //prevent form from submitting
        return false;
    });
});
