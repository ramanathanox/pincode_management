<?php
session_start();
if (isset($_SESSION['sessionuser'])) {
    header('location:adminarea.php');
}
include ('database.php');
$dbconnection = new Database();
$dbconnection->registration_details();
?>
<html>
    <head>
        <title>Registration Page</title>
        <link rel="stylesheet" type="text/css" href="pincodesearch.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="pincodesearch.js"></script>
    </head>
    <body>
        <header id="top">
            <div id="logo">
                <a href="index.php" title="OX Softwares">
                    <img height="auto" width="auto" src="upload/pinlogo.png" alt="" title="" />
                </a>
            </div>
            <div id="navigation">
                <ul id="menu-item">
                    <li id="menu-item-1"><a href="index.php"><span title="Home">HOME</span></a></li>
                    <li id="menu-item-2"><a href="login.php"><span title="Login">LOGIN</span></a></li>
                </ul>
            </div>
        </header>
        <div id="register">
            <div id="register-title"><h1>Registration</h1></div>
            <form id="form" method="post" enctype="multipart/form-data"> 
                <div id="register-page">
                    <div id="text-field">
                        <div class="text"><label > Name </label></div>
                        <div id="field"><input type="text" id="firstName" name="Firstname"  placeholder=" Name" required/></div>
                        <div id="error"><span id="firstNameerror"></span> </div>
                    </div> 
                    <div id="text-field">
                        <div class="text"><label>Email </label></div>
                        <div id="field"><input type="email" id="emailID" name="Mailid" placeholder=" Email ID" required/></div>
                        <div id="error"><span id="emailIDerror"></span></div>
                    </div>
                    <div id="text-field">
                        <div class="text"><label>Mobile </label></div>
                        <div id="field"> <input type="text" id="mobileNumber" name="Mobile" placeholder=" Mobile Number" required/></div>
                        <div id="error"><span id="mobileNumbererror"></span></div>
                    </div>
                    <div id="text-field">
                        <div class="text"><label>Password </label></div>
                        <div id="field"><input type="password" id="password" name="Password" placeholder=" Password" required/></div>
                        <div id="error"><span id="passworderror"></span></div>
                    </div> 
                    <div id="text-field">
                        <div class="text"> <label>Confirm-Password</label></div>
                        <div id="field"><input type="password" id="confirmpassword" name="confirmPassword" placeholder="Confirm-Password" required/></div>
                        <div id="error"><span id="confirmpassworderror"></span></div>
                        <div id="error"><span id="matchedpassworderror" ></span></div>
                    </div>
                    <div id="text-field">
                        <input type="submit" name="register-submit" id="register-submit" value="SUBMIT" />

                        <p class="notmember">already a member <a href="login.php" id="login-here">Login here!</a> </p>
                    </div>
                </div>
            </form>
        </div>
        <footer id="bottom">
            <div id="connect-with-us">CONNECT WITH US</div>
            <div id="media">  
                <ul id="socialmedia">
                    <li><a href="https://www.facebook.com/oxsoftwares" target="_blank" title="Find us on Facebook">Facebook</a></li>
                    <li><a href="https://twitter.com/oxsoftwares" target="_blank" title="Find us on Twitter">Twitter</a></li>
                    <li><a href="https://plus.google.com/+oxsoftwares" target="_blank" title="Find us on Google+">Google+</a></li>
                    <li><a href="https://www.linkedin.com/company/oxsoftwares" target="_blank" title="Find us on LinkedIn">LinkedIn</a></li>
                </ul>
            </div>
            <div id="newsletter">NEWSLETTER</div>
            <div id="signup">Sign up for our newsletter to get news and updates.</div>
            <div id="subscribe">
                <form method="post" id="newsletter-signup" action="subscribe.php?action=signup">
                    <input type="email" name="signup-email" id="newsletter-email" placeholder="Enter email to subscribe" required/>
                    <input type="submit" id="newsletter-submit" title="Subscribe" value="SUBSCRIBE" />
                    <div id="err"><span id="signup-response"></span></div>
                </form>
            </div>
        </div>
        <div id="copyright">
            2017 &copy; OX SoftwareS. All rights reserved.
        </div>
    </footer>
</body>
</html>
